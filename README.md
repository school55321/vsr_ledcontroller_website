# Getting started

## Requirements

- NodeJS installed (v18.16.0)
- Yarn installed (corepack)
- nvm (optional)

## Installing

1. `nvm use` (optional => requires nvm)
2. `yarn install`

## Running

1. `nvm use` (optional => requires nvm)
2. `yarn start`