import {asyncTimeout, createOfflineError, createParamError, errorResponse, successResponse} from "./helper";
import {logger} from "./server";
import {Request, Response} from "express";
import {type} from "node:os";

const LAST_SEEN_LIMIT = 60 * 60 * 4; // 60sec * 60 minutes * 4 hours

enum PoweredDeviceState {
    UNKNOWN,
    OFF,
    RECENTLY_SEEN,
    STALE,
}

interface PoweredDevice {
    powerDevice: string,
    state: PoweredDeviceState,
    startupDelay: number,
    lastSeen: null | Date,
}

const powerLookup: Record<string, PoweredDevice> = {
    "10.42.41.39": {
        powerDevice: "http://10.42.41.40",
        state: PoweredDeviceState.UNKNOWN,
        startupDelay: 10000,
        lastSeen: null,
    },
    "10.42.41.201": {
        powerDevice: "https://controller3.intern.stormson.com",
        state: PoweredDeviceState.UNKNOWN,
        startupDelay: 10000,
        lastSeen: null,
    },
    "10.42.41.202": {
        powerDevice: "https://controller2.intern.stormson.com",
        state: PoweredDeviceState.UNKNOWN,
        startupDelay: 10000,
        lastSeen: null,
    },
}

export enum PowerActions {
    NOTHING,
    FAILED,
    POWER_ON,
    POWER_CYCLE,
}

enum PlugRequestAction {
    ON = "on",
    OFF = "off",
    TOGGLE = "toggle",
}

function isPlugRequestAction(state: string): state is PlugRequestAction {
    return Object.values(PlugRequestAction).includes(state as any);
}

export function requestPowerPlugChange(request: Request, response: Response) {
    const ip = request.params.ip;
    const state = request.body.state;
    if (!ip || !request.body || !isPlugRequestAction(state)) {
        response.writeHead(400, {'Content-Type': "application/json"});
        response.end(errorResponse(createParamError()));
        return;
    }


    setPowerPlug(`http://${ip}`, state)
        .then(() => {
            response.writeHead(200, {'Content-Type': "application/json"});
            response.end(successResponse());

            for (const controllerIp in powerLookup) {
                const poweredDevice = powerLookup[controllerIp];

                // IP is valid => else we couldn't communicate. But we don't know if it is registered
                // with http or https (and we use in this call always http). So we check if the ending
                // is the same (and left out the http part)
                if (poweredDevice.powerDevice.endsWith(ip)) {
                    switch (state) {
                        case PlugRequestAction.ON:
                            if (poweredDevice.state !== PoweredDeviceState.OFF) return;
                            poweredDevice.state = PoweredDeviceState.UNKNOWN;
                            return;
                        case PlugRequestAction.OFF:
                            poweredDevice.state = PoweredDeviceState.OFF;
                            return;
                        case PlugRequestAction.TOGGLE:
                            poweredDevice.state = PoweredDeviceState.UNKNOWN;
                            return;
                    }
                }
            }
        })
        .catch(() => {
            response.writeHead(400, {'Content-Type': "application/json"});
            response.end(errorResponse(createOfflineError()));
        })
}

export async function ensureOn(ip: string, testIsOn: (ip: string) => Promise<boolean>) {
    if (!(ip in powerLookup)) return PowerActions.NOTHING;
    const poweredDevice = powerLookup[ip];
    let poweredOn = false;

    logger.verbose("Ensuring device is on...", {ip});
    if (poweredDevice.state === PoweredDeviceState.OFF) {
        await changePoweredDevice(ip, PlugRequestAction.ON);
        poweredOn = true;
    }
    if (await testIsOn(ip)) return poweredOn ? PowerActions.POWER_ON : PowerActions.NOTHING;

    // Not working, lets try turning it off and back on again
    logger.info("Try turning it off and on again...", {ip});
    await changePoweredDevice(ip, PlugRequestAction.OFF);
    await asyncTimeout(3000);
    await changePoweredDevice(ip, PlugRequestAction.ON);
    return await testIsOn(ip) ? PowerActions.POWER_CYCLE : PowerActions.FAILED;
}

export async function changePoweredDevice(ip: string, state: PlugRequestAction) {
    if (!(ip in powerLookup)) throw "No power device";

    const poweredDevice = powerLookup[ip];
    await setPowerPlug(poweredDevice.powerDevice, state);

    if (state === PlugRequestAction.OFF) {
        poweredDevice.state = PoweredDeviceState.OFF;
        logger.verbose("Disabled power by turning off %s", poweredDevice.powerDevice, {ip});
    } else {
        poweredDevice.state = PoweredDeviceState.UNKNOWN;
        logger.verbose("Enabled or toggle power by turning on %s, waiting %s ms",
            poweredDevice.powerDevice,
            poweredDevice.startupDelay,
            {ip});
        await asyncTimeout(poweredDevice.startupDelay);
    }
}

export async function setPowerPlug(powerPlugIp: string, state: PlugRequestAction) {
    const response = await fetch(`${powerPlugIp}/relay/0?turn=${state}`);

    const obj = await response.json();
    if ("ison" in obj) {
        if ((state === PlugRequestAction.ON && obj["ison"] !== true) ||
            (state === PlugRequestAction.OFF && obj["ison"] !== false)
        ) {
            logger.warning("Failed to control", {ip: powerPlugIp});
            throw "Failed setting state";
        }
    }

    logger.verbose("Turned %s", state ? "on" : "off", {ip: powerPlugIp});
}

export function seenDevice(ip: string) {
    if (!(ip in powerLookup)) return;

    powerLookup[ip].lastSeen = new Date();
    powerLookup[ip].state = PoweredDeviceState.RECENTLY_SEEN;
}

export function getPowerState(controller: string): PoweredDevice {
    if (!(controller in powerLookup)) {
        return {
            state: PoweredDeviceState.UNKNOWN,
            powerDevice: "",
            startupDelay: 0,
            lastSeen: null,
        }
    }

    return structuredClone(powerLookup[controller]);
}

export function hasRecentlySeen(ip: string) {
    // Device is not a known powered device, but may still exist and be on. Returning 'true' to prevent trying to power cycle
    if (!(ip in powerLookup)) return true;

    const poweredDevice = powerLookup[ip];
    const now = new Date();

    switch (poweredDevice.state) {
        case PoweredDeviceState.UNKNOWN:
            return false;
        case PoweredDeviceState.OFF:
            return false;
        case PoweredDeviceState.RECENTLY_SEEN:
            if (poweredDevice.lastSeen === null) return false;
            if ((now.getDate() - poweredDevice.lastSeen.getDate()) / 1000 <= LAST_SEEN_LIMIT) return true;

            poweredDevice.state = PoweredDeviceState.STALE;
            return false;
        case PoweredDeviceState.STALE:
            return false;
    }
}