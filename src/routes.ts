import {Express} from "express";
import {changeStrip, getStripState} from "./led_controller";
import {requestPowerPlugChange} from "./power_managment";
import {executeTemplate, getTemplates, saveTemplate} from "./templating";

export function setupRoutes(app: Express) {
    app.post("/strip/:ip/control", changeStrip);
    app.get("/strip/:ip/state", getStripState);
    app.post("/power-plug/:ip", requestPowerPlugChange);
    app.post("/templates/:template/controller/:ip", saveTemplate);
    app.post("/templates/:template", executeTemplate);
    app.get("/templates", getTemplates)
}