import express, {NextFunction, Request, Response} from "express";
import dotenv from "dotenv";
import bodyParser from "body-parser";
import fs from "fs";
import path from "path";
import {setupRoutes} from "./routes";
import winston from "winston";

dotenv.config();
const app = express();
const port = process.env.PORT || 3000;
export const logger = winston.createLogger({
    level: "verbose",
    levels: {
        error: 0,
        warning: 1,
        http: 2,
        info: 3,
        verbose: 4,
    },
    transports: [
        new winston.transports.Console({
            format: winston.format.combine(
                winston.format.timestamp(),
                winston.format.splat(),
                winston.format.colorize({all: true}),
                winston.format.printf(({level, message, timestamp, ip}) => {
                    if (ip) return `${timestamp} [${level}] <${ip}>: ${message}`;
                    else return `${timestamp} [${level}]: ${message}`;
                })
            ),
        })
    ]
})

function renderStaticHtml(req: Request, res: Response, next: NextFunction) {
    const pathname = req.path === "/" ? "/index.html" : req.path;
    if (!pathname.endsWith(".html")) return next();

    const filepath = "." + pathname.substring(0, pathname.length - 4);
    if (fs.existsSync(path.join(app.get("views"), filepath + "ejs"))) return res.render(filepath + "ejs");
    return next();
}

export function initHttpServer() {
    app.set("views", "./src/public")
    winston.addColors({
        error: "red",
        warning: "yellow",
        http: "cyan",
        info: "green",
        verbose: "gray",
    })

    app.use(winstonLoggerMiddleware)
    app.use(renderStaticHtml);
    app.use(express.static("src/public"));
    app.use(bodyParser.json());
    setupRoutes(app);
}

function winstonLoggerMiddleware(request: Request, response: Response, next: NextFunction) {
    logger.http(`${request.method} ${request.url}`, {ip: request.ip});
    next();
}

export function startHttpServer() {
    app.listen(port, () => {
        logger.info(`Example app listening on port ${port}`)
    });
}