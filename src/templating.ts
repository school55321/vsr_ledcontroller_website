import {
    applyTemplate,
    ControllerColor,
    ControllerModes,
    ControllerState,
    createControllerColor,
    extractPattern,
    Pattern, testConnection
} from "./led_controller";
import {
    assertNever,
    createOfflineError,
    createParamError,
    errorResponse,
    loadFromFile,
    saveToFile,
    successResponse
} from "./helper";
import {Request, Response} from "express";
import {ensureOn, hasRecentlySeen, PowerActions} from "./power_managment";
import {logger} from "./server";


type OnlyModeRequest = {
    mode: ControllerModes.TEST | ControllerModes.OFF
};

type StaticRequest = ControllerColor & {
    mode: ControllerModes.ALL
};

type AnimationModes = ControllerModes.GLOW | ControllerModes.WALK;
type DualColorAnimationRequest = ControllerColor & {
    mode: AnimationModes,
    secondary: ControllerColor
};

type PatternRequest = {
    mode: ControllerModes.PATTERN
    patterns: Pattern[]
}

type NoModeRequest = {
    mode: "IGNORE"
}

export type Template = {
    speed?: number,
    leds?: number
} & (StaticRequest | DualColorAnimationRequest | PatternRequest | OnlyModeRequest | NoModeRequest)

const templates: Record<string, Record<string, Template>> = loadFromFile("templates.json");

export function createTemplate(body: Record<string, any>): Template | null {
    const speed = body.speed !== undefined ? body.speed : undefined;
    const leds = body.leds !== undefined ? body.leds : undefined;

    if (body.mode) {
        const mode = body.mode.toUpperCase();
        switch (mode) {
            case "OFF":
                return {mode: ControllerModes.OFF, leds, speed};
            case "TEST":
                return {mode: ControllerModes.TEST, leds, speed};
            case "IGNORE":
                return {mode: "IGNORE", leds, speed};
            case "ALL":
                const staticRequest = CreateTemplateForStaticMode({...body, mode});
                if (!staticRequest) return null;
                return {...staticRequest, leds, speed};
            case "GLOW":
            case "WALK":
                const dualColorAnimationRequest = CreateTemplateForDualColorAnimation({...body, mode});
                if (!dualColorAnimationRequest) return null;
                return {...dualColorAnimationRequest, leds, speed};
            case "SHOW_PAT":
                const patternRequest = CreateTemplateForPatternMode({...body, mode});
                if (!patternRequest) return null;
                return {...patternRequest, leds, speed};
        }
    } else {
        if (body.patterns) {
            const patternRequest = CreateTemplateForPatternMode({...body, mode: ControllerModes.PATTERN});
            if (!patternRequest) return null;
            return {...patternRequest, leds, speed};
        } else if (body.secondary) {
            const dualColorAnimationRequest = CreateTemplateForDualColorAnimation({
                ...body,
                mode: ControllerModes.GLOW
            });
            if (!dualColorAnimationRequest) return null;
            return {...dualColorAnimationRequest, leds, speed};
        } else if (body.rgb) {
            const staticRequest = CreateTemplateForStaticMode({...body, mode: ControllerModes.ALL});
            if (!staticRequest) return null;
            return {...staticRequest, leds, speed};
        } else {
            return {mode: ControllerModes.OFF, leds, speed};
        }
    }

    return null;
}

export function templateFromControllerState(state: ControllerState): Template {
    switch (state.mode) {
        case ControllerModes.OFF:
            return {
                mode: ControllerModes.OFF,
                leds: state.leds > 0 ? state.leds : undefined,
                speed: state.speed > 0 ? state.speed : undefined,
            }
        case ControllerModes.TEST:
            return {
                mode: ControllerModes.TEST,
                leds: state.leds > 0 ? state.leds : undefined,
                speed: state.speed > 0 ? state.speed : undefined,
            }
        case ControllerModes.ALL:
            return {
                mode: ControllerModes.ALL,
                leds: state.leds > 0 ? state.leds : undefined,
                speed: state.speed > 0 ? state.speed : undefined,
                ...state.colors[0],
            }
        case ControllerModes.WALK:
        case ControllerModes.GLOW:
            return {
                mode: state.mode,
                leds: state.leds > 0 ? state.leds : undefined,
                speed: state.speed > 0 ? state.speed : undefined,
                ...state.colors[0],
                secondary: {...state.colors[1]},
            }
        case ControllerModes.PATTERN:
            return {
                mode: ControllerModes.PATTERN,
                leds: state.leds > 0 ? state.leds : undefined,
                speed: state.speed > 0 ? state.speed : undefined,
                patterns: state.colors.map((color) => {
                    return {
                        ...color,
                        from: -1,
                        until: -1,
                    }
                })
            }
        default:
            return assertNever(state.mode);
    }
}

export function saveTemplate(request: Request, response: Response) {
    try {
        const templateName = request.params.template;
        const ip = request.params.ip;
        if (!templateName || !ip || !request.body) {
            response.writeHead(400, {'Content-Type': "application/json"});
            response.end(errorResponse(createParamError()));
            return;
        }

        const template = createTemplate(request.body)
        if (!template) {
            response.writeHead(400, {'Content-Type': "application/json"});
            response.end(errorResponse(createParamError()));
            return;
        }

        if (!templates[templateName]) templates[templateName] = {};
        templates[templateName][ip] = template;
        saveToFile("templates.json", templates);

        response.writeHead(200, {'Content-Type': "application/json"});
        response.end(successResponse());
    } catch {
        response.writeHead(400, {'Content-Type': "application/json"});
        response.end(errorResponse(createParamError()));
        return;
    }
}

export async function executeTemplate(request: Request, response: Response) {
    try {
        const templateName = request.params.template;
        if (!templateName || !templates[templateName]) {
            response.writeHead(400, {'Content-Type': "application/json"});
            response.end(errorResponse(createParamError()));
            return;
        }

        const template = templates[templateName];
        for (const ip in template) {
            await new Promise(async (resolve, reject) => {
                const onClose = (error: boolean) => {
                    if (error) reject();
                }

                const onData = (data: string) => {
                    resolve(data);
                }

                if (!hasRecentlySeen(ip)) {
                    const result = await ensureOn(ip, testConnection);
                    if (result === PowerActions.FAILED) {
                        logger.warning("Unable to turn on controller, bailing command");
                        response.writeHead(500, {'Content-Type': "application/json"});
                        response.end(errorResponse(createOfflineError()));
                        return;
                    }

                    logger.verbose("Controller is online, send template");
                }

                applyTemplate(template[ip], ip, onClose, onData, response);
            });
        }

        response.writeHead(200, {'Content-Type': "application/json"});
        response.end(successResponse());
    } catch {
        response.writeHead(400, {'Content-Type': "application/json"});
        response.end(errorResponse(createParamError()));
        return;
    }
}

export async function getTemplates(request: Request, response: Response) {
    response.writeHead(200, {'Content-Type': "application/json"});
    response.end(successResponse({"templates": templates}));
}

function CreateTemplateForStaticMode(body: Record<string, any> & { mode: ControllerModes.ALL }): Template | null {
    const primary = createControllerColor(body);
    if (!primary) return null;

    return {
        mode: ControllerModes.ALL,
        ...primary,
    }
}

function CreateTemplateForDualColorAnimation(body: Record<string, any> & { mode: AnimationModes }): Template | null {
    if (!body.secondary) return null;

    const primary = createControllerColor(body);
    const secondary = createControllerColor(body.secondary);
    if (!primary || !secondary) return null;

    return {
        mode: body.mode,
        ...primary,
        secondary: {...secondary}
    }
}

function CreateTemplateForPatternMode(body: Record<string, any> & { mode: ControllerModes.PATTERN }): Template | null {
    const pattern = extractPattern(body);
    if (!pattern) return null;

    return {
        mode: ControllerModes.PATTERN,
        patterns: pattern,
    };
}
