import fs from "fs";
import {logger} from "./server";
import {ControllerColor, Pattern} from "./led_controller";

export const ParamsErrorStr = "PARAM_ERR"
export type ParamsError = {
    type: typeof ParamsErrorStr,
    code: 1,
}
export const createParamError = (): ParamsError => {
    return {type: ParamsErrorStr, code: 1}
}

export const CommunicationErrorStr = "COMM_ERR"
export type CommunicationError = {
    type: typeof CommunicationErrorStr,
    code: 2,
}
export const createCommunicationError = (): CommunicationError => {
    return {type: CommunicationErrorStr, code: 2}
}

export const OfflineErrorStr = "OFFLINE_ERR"
export type OfflineError = {
    type: typeof OfflineErrorStr,
    code: 3,
}
export const createOfflineError = (): OfflineError => {
    return {type: OfflineErrorStr, code: 3}
}

export type Error = ParamsError | CommunicationError | OfflineError;

export function loadFromFile(file: string) {
    try {
        return JSON.parse(fs.readFileSync(file).toString())
    } catch (err) {
        return {}
    }
}

export function saveToFile(file: string, obj: Record<string, any>) {
    fs.writeFileSync(file, JSON.stringify(obj));
}

export function errorResponse(error: Error) {
    return JSON.stringify({"status": false, "reason": error.code});
    // switch (error.type) {
    //     case ParamsErrorStr:
    //         return JSON.stringify({"status": false, "reason": 1});
    //     case CommunicationErrorStr:
    //         return JSON.stringify({"status": false, "reason": 2});
    //     case OfflineErrorStr:
    //         return JSON.stringify({"status": false, "reason": 3})
    // }
}


export function successResponse(data?: Record<string, any>) {
    if (data) return JSON.stringify({...data, "status": true});
    else return JSON.stringify({"status": true});
}

export function toColorFormat(color: ControllerColor) {
    const r = parseInt(color.rgb.substring(0, 2), 16).toString().padStart(3, "0");
    const g = parseInt(color.rgb.substring(2, 4), 16).toString().padStart(3, "0");
    const b = parseInt(color.rgb.substring(4, 6), 16).toString().padStart(3, "0");
    const w = color.white.toString().padStart(3, "0");
    return r + g + b + w;
}

export function toPatternFormat(pattern: Pattern) {
    return `${toColorFormat(pattern)}${pattern.from}${pattern.until}`;
}

export async function asyncTimeout(ms: number): Promise<void> {
    return await (new Promise((resolve) => setTimeout(() => resolve(), ms)));
}

export function assertNever(x: never): never {
    throw new Error("Unexpected object: " + x);
}