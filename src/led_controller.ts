import {Request, Response} from "express"
import net from "net";
import {
    assertNever,
    createCommunicationError,
    createOfflineError,
    createParamError,
    errorResponse,
    loadFromFile,
    saveToFile,
    successResponse,
    toColorFormat,
    toPatternFormat
} from "./helper";
import {logger} from "./server";
import {ensureOn, getPowerState, hasRecentlySeen, PowerActions, seenDevice} from "./power_managment";
import {createTemplate, Template, templateFromControllerState} from "./templating";

export enum ControllerModes {
    OFF = "OFF",
    ALL = "ALL",
    PATTERN = "PATTERN",
    TEST = "TEST",
    GLOW = "GLOW",
    WALK = "WALK",
}

export interface ControllerColor {
    rgb: string,
    white: number,
}

export type Pattern = ControllerColor & {
    from: number,
    until: number
}

export interface ControllerState {
    leds: number,
    speed: number,
    mode: ControllerModes,
    colors: [ControllerColor, ControllerColor, ControllerColor, ControllerColor]
}

const states: Record<string, ControllerState> = loadFromFile("controller-states.json");

const newControllerState = (): ControllerState => {
    return {
        mode: ControllerModes.OFF,
        leds: -1,
        speed: 0,
        colors: [
            {rgb: "000000", white: 0},
            {rgb: "000000", white: 0},
            {rgb: "000000", white: 0},
            {rgb: "000000", white: 0},
        ],
    }
}

export function isControllerMode(mode: string): mode is ControllerModes {
    return Object.values(ControllerModes).includes(mode as ControllerModes);
}

export function sendCommandTo(ip: string, command: string, onClose: (error: boolean) => void, onData: (data: string) => void) {
    const client = new net.Socket();
    logger.verbose("Connecting to '%s'", ip);
    const timer = setTimeout(() => failedToConnect(client, onClose), 5000);

    client.once("data", (data) => {
        clearTimeout(timer);
        onSocketData(client, ip, data, onData)
    });
    client.once("error", (err) => onSocketError(client, ip, onClose, err));
    client.once("close", () => onSocketClose(client, ip, onClose));
    client.connect(8080, ip, () => {
        logger.verbose("Connected, sending '%s'", command, {ip});
        client.write(command + "\r\n", "ascii");
    })
}

export function changeStrip(request: Request, response: Response) {
    try {
        const ip = request.params.ip;
        if (!ip) {
            response.writeHead(400, {'Content-Type': "application/json"});
            response.end(errorResponse(createParamError()));
            return;
        }
        if (!states[ip]) {
            states[ip] = newControllerState();
        }

        const onClose = (error: boolean) => {
            if (!response.writableEnded) {
                logger.info('Inform client of failure connection');
                response.writeHead(500, {'Content-Type': 'application/json'})
                response.end(errorResponse(createCommunicationError()));
            }
        }
        const onData = (data: string) => {
            if (!response.writableEnded) {
                seenDevice(ip);
                response.writeHead(200, {'Content-Type': "application/json"});
                response.end(successResponse({response: data}));
            }
        }

        if (!hasRecentlySeen(ip)) {
            ensureOn(ip, testConnection)
                .then((result) => {
                    if (result === PowerActions.FAILED) {
                        logger.warning("Unable to turn on controller, bailing command");
                        response.writeHead(500, {'Content-Type': "application/json"});
                        response.end(errorResponse(createOfflineError()));
                        return;
                    }

                    logger.verbose("Controller is online, send command");
                    sendStripCommands(request.body, ip, onClose, onData, response);
                })
                .catch(() => {
                    logger.warning("Unable to turn on controller, bailing command");
                    response.writeHead(500, {'Content-Type': "application/json"});
                    response.end(errorResponse(createOfflineError()));
                });
        } else {
            sendStripCommands(request.body, ip, onClose, onData, response);
        }
    } catch {
        response.writeHead(400, {'Content-Type': "application/json"});
        response.end(errorResponse(createParamError()));
        return;
    }
}

export function getStripState(request: Request, response: Response) {
    try {
        const ip = request.params.ip;
        if (!ip) {
            response.writeHead(400, {'Content-Type': "application/json"});
            response.end(errorResponse(createParamError()));
            return;
        }
        if (!states[ip]) {
            const empty = newControllerState();
            response.writeHead(200, {'Content-Type': "application/json"});
            response.end(successResponse({state: empty}));
            return;
        }

        response.writeHead(200, {'Content-Type': "application/json"});
        response.end(successResponse({
            state: states[ip],
            power_state: getPowerState(ip),
        }));
        return;
    } catch {
        response.writeHead(400, {'Content-Type': "application/json"});
        response.end(errorResponse(createParamError()));
        return;
    }
}

function failedToConnect(socket: net.Socket, onClose: (error: boolean) => void) {
    onClose(true);
    socket.destroy();
}

function onSocketData(socket: net.Socket, ip: string, data: Buffer, onData: (data: string) => void) {
    logger.verbose('Data received %s', data, {ip});
    onData(data.toString());
    socket.destroy();
}

function onSocketError(socket: net.Socket, ip: string, onClose: (error: boolean) => void, err: Error) {
    logger.warning("Connection error => '%s'", err, {ip});
    onClose(true);
    socket.destroy();
}

function onSocketClose(socket: net.Socket, ip: string, onClose: (error: boolean) => void) {
    logger.verbose('Connection closed', {ip});
    onClose(false);
}

export function createControllerColor(data: Record<string, any>): ControllerColor | null {
    if (!data.rgb) return null;
    if (data.white === undefined) return null;

    return {
        rgb: data.rgb,
        white: parseInt(data.white),
    }
}

export function extractPattern(data: Record<string, any>): Pattern[] | null {
    if (!data.patterns) return null;
    const patterns: Pattern[] = [];
    let end = 0;

    for (let pattern of data.patterns) {
        const color = createControllerColor(pattern);
        if (!color) continue;
        if (pattern.from === undefined || !pattern.until) continue;

        end = Math.max(end, pattern.until);
        const from = pattern.from.toString().padStart(3, '0');
        const until = pattern.until.toString().padStart(3, '0');
        patterns.push({...color, from, until});
    }

    if (patterns.length === 0) return null;

    // Requires always 4 patterns, fill remaining patterns from the start with 'black' at the end of the strip
    const lastIndex = (end + 1);
    const dark: Pattern = {rgb: "000000", white: 0, until: lastIndex, from: lastIndex};

    patterns.reverse();
    for (let i = patterns.length; i < 4; i++) {
        patterns.push(dark);
    }
    patterns.reverse();
    return patterns;
}

function sendStripCommands(body: Record<string, any>, ip: string, onClose: (error: boolean) => void, onData: (data: string) => void, response: Response) {
    let template = createTemplate(body);
    if (!template) {
        response.writeHead(400, {'Content-Type': "application/json"});
        response.end(errorResponse(createParamError()));
        return;
    }

    applyTemplate(template, ip, onClose, onData, response);
}

export function applyTemplate(template: Template, ip: string, onClose: (error: boolean) => void, onData: (data: string) => void, response: Response) {
    const calls: (() => void)[] = [];

    const checkEndOfChain = (data: string) => {
        if (calls.length === 0) return onData(data);
    }

    const onConnectionClosed = (error: boolean) => {
        if (error) return onClose(error);

        const call = calls.pop();
        if (!call) return onClose(error);

        // Give some time for controller to process disconnection
        setTimeout(call, 150);
    }

    if (!states[ip]) {
        states[ip] = newControllerState();
    }

    if (template.mode === ControllerModes.PATTERN) {
        for (let i = 0; i < 4; i++) {
            const pattern = template.patterns[i];
            calls.push(() => {
                sendCommandTo(ip, `ADD_PAT${i}${toPatternFormat(pattern)}`, onConnectionClosed, checkEndOfChain);
                states[ip].colors[i] = {rgb: pattern.rgb, white: pattern.white};
            })
        }
    }

    if (template.leds) {
        // This will also 'auto' refresh non-animation modes when increasing size
        if (template.mode === "IGNORE") {
            const oldTemplate = template;
            template = templateFromControllerState(states[ip]);
            template.leds = oldTemplate.leds || -1;
            template.speed = oldTemplate.speed;
        }

        if (template.leds < states[ip].leds) {
            calls.push(() => {
                // First turn all off, so when making strip 'smaller' all the other leds turn cleanly off
                sendCommandTo(ip, ControllerModes.OFF, onConnectionClosed, checkEndOfChain)
            })
        }

        calls.push(() => {
            states[ip].leds = template.leds || -1;
            sendCommandTo(ip, `LEDS${states[ip].leds}`, onConnectionClosed, checkEndOfChain);
        })
    }

    if (template.speed) calls.push(() => {
        states[ip].speed = template.speed || 0;
        sendCommandTo(ip, `SPEED${states[ip].speed}`, onConnectionClosed, checkEndOfChain);
    })

    if (template.mode !== "IGNORE") {
        calls.push(() => {
            states[ip].mode = template.mode;

            switch (template.mode) {
                case ControllerModes.OFF:
                    sendCommandTo(ip, template.mode, onConnectionClosed, checkEndOfChain);
                    return;
                case ControllerModes.TEST:
                    sendCommandTo(ip, template.mode, onConnectionClosed, checkEndOfChain);
                    return;
                case ControllerModes.ALL:
                    sendCommandTo(ip, `${template.mode}${toColorFormat(template)}`,
                        onConnectionClosed, checkEndOfChain);
                    states[ip].colors[0] = {rgb: template.rgb, white: template.white};
                    return;
                case ControllerModes.GLOW:
                case ControllerModes.WALK:
                    sendCommandTo(ip,
                        `${template.mode}${toColorFormat(template)} ${toColorFormat(template.secondary)}`,
                        onConnectionClosed, checkEndOfChain);
                    states[ip].colors[0] = {rgb: template.rgb, white: template.white};
                    states[ip].colors[1] = {rgb: template.secondary.rgb, white: template.secondary.white};
                    return;

                case ControllerModes.PATTERN:
                    sendCommandTo(ip, "SHOW_PAT", onConnectionClosed, checkEndOfChain);
                    return;

                default:
                    assertNever(template);
            }
        })
    }

    calls.push(() => {
        saveToFile("controller-states.json", states);
        checkEndOfChain("DONE");
        onConnectionClosed(false);
    });

    calls.reverse();
    onConnectionClosed(false);
}

export function testConnection(ip: string): Promise<boolean> {
    return new Promise((resolve, reject) => {
        const onFailure = (error: boolean) => {
            if (error) resolve(false);
        }
        const onData = () => {
            resolve(true);
        }

        sendCommandTo(ip, "CONN_TEST", onFailure, onData)
    })
}