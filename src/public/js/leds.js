const elements = {};
const mElements = {};

addInitScript(async () => {
    const selectedMode = getElementById("func");
    selectedMode.addEventListener("change", async () => await updateView());

    const selectedIp = getElementById("ip");
    selectedIp.onchange = async () => await updateView(true);

    const btnExecute = getElementById("execute");
    btnExecute.onclick = async () => await execute();

    const txtAdjustSize = getElementById("txt-leds");
    txtAdjustSize.addEventListener("keydown", (e) => {
        if (e.key.toLowerCase() === "enter") adjustSize();
    });

    const txtAdjustSpeed = getElementById("txt-speed");
    txtAdjustSpeed.addEventListener("keydown", (e) => {
        if (e.key.toLowerCase() === "enter") adjustSpeed();
    });


    const templateName = getElementById("txt-template-name");
    mElements["txt-template-name"] = M.Autocomplete.init([templateName], {
        minLength: 0,
        maxDropDownHeight: "150px",
        data: [],
    })[0];

    await updateView(true);
});

function setVisibilityBox(box, visible, position = false) {
    const colorBox = getElementById(`${box}-color-box`);
    const positionBox = getElementById(`${box}-position-box`);

    if (visible) {
        colorBox.classList.remove("hidden");
        if (position) positionBox.classList.remove("hidden");
        else positionBox.classList.add("hidden");
    } else {
        colorBox.classList.add("hidden");
        positionBox.classList.add("hidden");
    }
}

async function updateView(loadState = false) {
    const selectedMode = getElementById("func");

    if (loadState) {
        setWorking(true);
        const state = await fetchState();
        await getTemplates();

        setWorking(false);
        setValues(state);
        selectedMode.value = state.mode;
        selectedMode.dispatchEvent(new Event("change"));
        return;
    }

    switch (selectedMode.value) {
        case "ALL":
            setVisibilityBox("primary", true);
            setVisibilityBox("secondary", false);
            setVisibilityBox("tertiary", false);
            setVisibilityBox("quaternary", false);
            return;
        case "PATTERN":
            setVisibilityBox("primary", true, true);
            setVisibilityBox("secondary", true, true);
            setVisibilityBox("tertiary", true, true);
            setVisibilityBox("quaternary", true, true);
            return;
        case "GLOW":
            setVisibilityBox("primary", true, false);
            setVisibilityBox("secondary", true, false);
            setVisibilityBox("tertiary", false, false);
            setVisibilityBox("quaternary", false, false);
            return;
        case "WALK":
            setVisibilityBox("primary", true, false);
            setVisibilityBox("secondary", true, false);
            setVisibilityBox("tertiary", false, false);
            setVisibilityBox("quaternary", false, false);
            return;
        default:
        case "OFF":
            setVisibilityBox("primary", false);
            setVisibilityBox("secondary", false);
            setVisibilityBox("tertiary", false);
            setVisibilityBox("quaternary", false);
            return;
    }
}

async function fetchState() {
    const result = await fetch(`/strip/${ip.value}/state`, {
        method: "GET",
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json",
        },
    });
    const data = await result.json();
    return data.state;
}

function setWorking(working) {
    const btnExecute = getElementById("execute");
    const loader = getElementById("busy-executing");

    if (working) {
        btnExecute.classList.add("disabled", "busy");
    } else {
        btnExecute.classList.remove("disabled", "busy");
    }
}

function getRequestObject(selectedMode, requestObject) {
    const {
        primaryColor, primaryWhite, primaryFrom, primaryUntil,
        secondaryColor, secondaryWhite, secondaryFrom, secondaryUntil,
        tertiaryColor, tertiaryWhite, tertiaryFrom, tertiaryUntil,
        quaternaryColor, quaternaryWhite, quaternaryFrom, quaternaryUntil,
    } = gatherValues();

    switch (selectedMode.value) {
        case "ALL":
            return {
                "rgb": primaryColor,
                "white": primaryWhite,
            };
        case "PATTERN":
            return {
                "patterns": [
                    {"rgb": primaryColor, "white": primaryWhite, "from": primaryFrom, "until": primaryUntil},
                    {"rgb": secondaryColor, "white": secondaryWhite, "from": secondaryFrom, "until": secondaryUntil},
                    {"rgb": tertiaryColor, "white": tertiaryWhite, "from": tertiaryFrom, "until": tertiaryUntil},
                    {
                        "rgb": quaternaryColor,
                        "white": quaternaryWhite,
                        "from": quaternaryFrom,
                        "until": quaternaryUntil
                    },
                ]
            };
        case "OFF":
            return {};
        case "TEST":
            return {
                "mode": "TEST",
            }
        default:
            return {
                "mode": selectedMode.value,
                "rgb": primaryColor,
                "white": primaryWhite,
                "secondary": {
                    "rgb": secondaryColor,
                    "white": secondaryWhite
                },
            }
    }
}

async function execute() {
    setWorking(true);

    const ip = getElementById("ip");
    const selectedMode = getElementById("func");
    const requestObject = getRequestObject(selectedMode);

    const result = await fetch(`/strip/${ip.value}/control`, {
        method: "POST",
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json",
        },
        body: JSON.stringify(requestObject),
    });
    setWorking(false);
}

function gatherValues() {
    const primaryColor = getElementById("primary-color").value.substring(1);
    const primaryWhite = getElementById("primary-white").value;
    const primaryFrom = getElementById("primary-from").value;
    const primaryUntil = getElementById("primary-until").value;

    const secondaryColor = getElementById("secondary-color").value.substring(1);
    const secondaryWhite = getElementById("secondary-white").value;
    const secondaryFrom = getElementById("secondary-from").value;
    const secondaryUntil = getElementById("secondary-until").value;

    const tertiaryColor = getElementById("tertiary-color").value.substring(1);
    const tertiaryWhite = getElementById("tertiary-white").value;
    const tertiaryFrom = getElementById("tertiary-from").value;
    const tertiaryUntil = getElementById("tertiary-until").value;

    const quaternaryColor = getElementById("quaternary-color").value.substring(1);
    const quaternaryWhite = getElementById("quaternary-white").value;
    const quaternaryFrom = getElementById("quaternary-from").value;
    const quaternaryUntil = getElementById("quaternary-until").value;

    return {
        primaryColor, primaryWhite, primaryFrom, primaryUntil,
        secondaryColor, secondaryWhite, secondaryFrom, secondaryUntil,
        tertiaryColor, tertiaryWhite, tertiaryFrom, tertiaryUntil,
        quaternaryColor, quaternaryWhite, quaternaryFrom, quaternaryUntil,
    };
}

function setValues(state) {
    getElementById("primary-color").value = `#${state["colors"][0]["rgb"]}`;
    getElementById("secondary-color").value = `#${state["colors"][1]["rgb"]}`;
    getElementById("tertiary-color").value = `#${state["colors"][2]["rgb"]}`;
    getElementById("quaternary-color").value = `#${state["colors"][3]["rgb"]}`;

    getElementById("primary-white").value = state["colors"][0]["white"];
    getElementById("secondary-white").value = state["colors"][1]["white"];
    getElementById("tertiary-white").value = state["colors"][2]["white"];
    getElementById("quaternary-white").value = state["colors"][3]["white"];

    if (state["leds"] > 0) getElementById("txt-leds").value = state["leds"];
    if (state["speed"] >= 0) getElementById("txt-speed").value = state["speed"];
}

function getElementById(id) {
    if (!(id in elements)) elements[id] = document.getElementById(id);
    return elements[id];
}

async function openSetting(setting) {
    switch (setting) {
        case "strip_size":
            const modalSize = getElementById("adjust-strip-size-dialog");
            modalSize.showModal();
            modalSize.focus();
            modalSize.blur();
            return;
        case "animation_speed":
            const modalSpeed = getElementById("adjust-animation-speed-dialog");
            modalSpeed.showModal();
            modalSpeed.focus();
            modalSpeed.blur();
            return;
        case "save_template":
            const modalAddTemplate = getElementById("add-to-template-dialog");
            modalAddTemplate.showModal();
            modalAddTemplate.focus();
            modalAddTemplate.blur();
            updateAutoComplete(await getTemplates());
            return;
    }
}

async function adjustSize() {
    const size = getElementById("txt-leds").value;
    if (size <= 0) return;


    setWorking(true);
    getElementById("adjust-strip-size-dialog").close();
    const response = await fetch(`/strip/${ip.value}/control`, {
        method: "POST",
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json",
        },
        body: JSON.stringify({
            "mode": "IGNORE",
            "leds": size,
        })
    });
    setWorking(false);
}

async function adjustSpeed() {
    const speed = getElementById("txt-speed").value;
    if (speed < 0) return;


    setWorking(true);
    getElementById("adjust-animation-speed-dialog").close();
    const response = await fetch(`/strip/${ip.value}/control`, {
        method: "POST",
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json",
        },
        body: JSON.stringify({
            "mode": "IGNORE",
            "speed": speed,
        })
    });
    setWorking(false);
}

async function addTemplate() {
    setWorking(true);

    const ip = getElementById("ip");
    const selectedMode = getElementById("func");
    const templateName = getElementById("txt-template-name");
    const requestObject = getRequestObject(selectedMode);

    const speed = getElementById("txt-speed").value;
    if (speed >= 0) requestObject.speed = speed;

    const size = getElementById("txt-leds").value;
    if (size > 0) requestObject.leds = size;

    getElementById("add-to-template-dialog").close();

    await fetch(`/templates/${templateName.value}/controller/${ip.value}`, {
        method: "POST",
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json",
        },
        body: JSON.stringify(requestObject)
    });
    setWorking(false);
}

function updateAutoComplete(templates ) {
    if ("txt-template-name" in mElements) {
        const autoCompleteData = [];
        for (const template in templates) {
            autoCompleteData.push({id: template})
        }
        mElements["txt-template-name"].setMenuItems(autoCompleteData);
    }
    return templates;
}

async function getTemplates() {
    const response = await fetch(`/templates`, {
        method: "GET",
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json",
        }
    });
    const data = await response.json();
    if (!data.status) throw "Unable to fetch templates";
    return data.templates;
}

