const initScripts = [];
let init = false;

function docReady(fn) {
    if (document.readyState === "complete" || document.readyState === "interactive") {
        setTimeout(fn, 1);
    } else {
        document.addEventListener("DOMContentLoaded", fn);
    }
}

docReady(() => {
    init = true;
    window.addEventListener('unhandledrejection', (e) => {
        console.error("Error occurred: " + e.reason.message);
    });

    document.documentElement.setAttribute('theme', 'stormson');
    M.AutoInit(document.getElementById("root"));

    for (let script of initScripts) {
        script();
    }
})

function addInitScript(script) {
    if (init) return script();
    initScripts.push(script);
}
