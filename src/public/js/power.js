async function setPowerState(address, state) {
    let controllerAddress = address;
    if (!address.startsWith("10.")) controllerAddress += ".intern.stormson.com";

    await fetch(`/power-plug/${controllerAddress}`, {
        method: "POST",
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json",
        },
        body: JSON.stringify({state,})
    });
}

async function setRoomLights(state) {
    await setPowerState("steen-licht", state);
    await setPowerState("kunst-licht", state);
}