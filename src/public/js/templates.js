addInitScript(async () => {
    const templates = await getTemplates();
    const container = document.getElementById('template-button-container');

    for (let templateName in templates) {
        const button = createTemplateButton(templateName);
        container.appendChild(button);
    }
})

async function getTemplates() {
    const response = await fetch(`/templates`, {
        method: "GET",
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json",
        }
    });
    const data = await response.json();
    if (!data.status) throw "Unable to fetch templates";
    return data.templates;
}

function createTemplateButton(templateName) {
    const el = document.createElement("div");
    el.classList.add("btn", "low-profile-color", "justify-center");
    el.innerText = templateName;
    el.onclick = async () => {
        await fetch(`/templates/${templateName}`, {
            method: "POST",
            headers: {
                "Accept": "application/json",
                "Content-Type": "application/json",
            }
        });
    }
    return el;
}